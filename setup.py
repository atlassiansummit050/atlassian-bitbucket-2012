#! /usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
import os.path
import re
from distutils.core import setup
from distutils.command.build import build
import ConfigParser


class my_build(build):
    def run(self):
        os.chdir("po")
        os.system("./l10n.py --update")
        os.system("./l10n.py --install")
        os.chdir("..")
        build.run(self)


def linguas():
   linguas = []
   listdir = os.listdir("po")
   for file in listdir:
       if re.search("^.*\.po$", file):
           linguas.append(file[:-3])
   return linguas

def get_data_files():
    data_files = []
    data_files.append(("pythonsudoku", ["data/FreeSans.ttf"]))

    for lingua in linguas():
        data_files.append(("%s/%s/%s" % ("locale", lingua, "LC_MESSAGES"),
                           ["locale/%s/LC_MESSAGES/pythonsudoku.mo" % lingua]))
    return data_files


setup(name="pythonsudoku",
      version="0.13",
      description="A graphical and text-based sudoku game",
      long_description="Python Sudoku is a text and graphical (gtk interface) program to create or resolve sudokus. It can also print a sudoku (1 or 4 sudokus in each page) and write an image (png, jpeg, etc) with a sudoku.",
      keywords="sudoku",
      license="GNU General Public License (GPL)",

      classifiers=["Development Status :: 5 - Production/Stable",
                   "Environment :: Console (Text Based)",
                   "Environment :: X11 Applications :: GTK",
                   "Intended Audience :: End Users/Desktop",
                   "License :: OSI Approved :: GNU General Public License (GPL)",
                   "Operating System :: OS Independent",
                   "Programming Language :: Python",
                   "Topic :: Games/Entertainment :: Puzzle Games",
                   ],

      url="http://pythonsudoku.sf.net",
      download_url="http://dl.sourceforge.net/sourceforge/pythonsudoku/pythonsudoku-0.13.zip",

      author="Xosé Otero",
      author_email="xoseotero@users.sourceforge.net",

      scripts=["pysdk.py", "pysdk-pdf.py", "pysdk-image.py", "pysdk-gui.py"],
      packages=["pythonsudoku"],
      data_files=get_data_files(),

      cmdclass={"build": my_build},
      )
